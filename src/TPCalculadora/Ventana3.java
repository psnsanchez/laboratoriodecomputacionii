package TPCalculadora;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Ventana3 extends JFrame{
    private JButton botn;
    private JPanel numeros;
    private boolean limpiarEtiqueta=true;
    private String operacion="=";
    private double total;
    private JLabel etiqueta =new JLabel("0",SwingConstants.RIGHT);
    public  Ventana3(){
        setTitle("Calculadora");
        setBounds(350,350,350,350);
        setVisible(true);
        Panel panel = new Panel();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        add(panel);
    }

    class Panel extends JPanel{

        public Panel(){
            setLayout(new BorderLayout());
            etiqueta.setBounds(100,0,20,20);         //setear tamaño y posicion
            etiqueta.setForeground(Color.red);                       //asignar un color al texto
            etiqueta.setOpaque(true);
            etiqueta.setBackground(Color.BLACK);                    //color de fondo
            etiqueta.setFont(new Font("arial",1,25));//fuente
            add(etiqueta,BorderLayout.NORTH);                       //posicion layout de etiqueta
            numeros = new JPanel();
            numeros.setLayout(new GridLayout(4,4));         //una matriz grafica de 4*4
            borrarPantalla borrar = new borrarPantalla();           //instancio metodo borrar
            Oyente oyente = new Oyente();                           //instancio metodo oyente numero
            Operadores operador = new Operadores();                 //instancio metodo de operacion

            //inicializo botones en el panel de botones, con un texto y un metodo de escucha
            iniciarBtn("1",oyente);
            iniciarBtn("2",oyente);
            iniciarBtn("3",oyente);
            iniciarBtn("+", operador);

            iniciarBtn("4", oyente);
            iniciarBtn("5", oyente);
            iniciarBtn("6", oyente);
            iniciarBtn("-", operador);

            iniciarBtn("7", oyente);
            iniciarBtn("8", oyente);
            iniciarBtn("9", oyente);
            iniciarBtn("*", operador);

            iniciarBtn("AC", borrar);
            iniciarBtn("0",oyente);
            iniciarBtn("=", operador);
            iniciarBtn("/", operador);

            add(numeros);
        }

        private class Oyente implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e){
                String numero = e.getActionCommand();
                if(limpiarEtiqueta){
                    etiqueta.setText("");
                    limpiarEtiqueta=false;
                }
                etiqueta.setText(etiqueta.getText()+numero);
            }
        }

        private class Operadores implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                String dato = e.getActionCommand();
                operacion(Double.parseDouble(etiqueta.getText()));
                operacion=dato;
                limpiarEtiqueta=true;
            }

            public void operacion(Double numero){//dependiendo operacion realiza accion
                try{
                    if (operacion.equals("+")){
                        total+=numero;
                    } else if (operacion.equals("-")){
                        total-=numero;
                    } else if (operacion.equals("*")){
                        total*=numero;
                    } else if (operacion.equals("/")){
                        total/=numero;
                    } else if (operacion.equals("=")){
                        total=numero;
                    } else if (operacion.equals("AC")){
                        total=0;
                        etiqueta.setText("0");
                    }
                    etiqueta.setText(total+"");
                } catch (ArithmeticException e){
                    System.out.println("Error, Division por cero");
                }
            }
        }

        private class borrarPantalla implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                etiqueta.setText(" ");
            }
        }
        private void iniciarBtn(String nombre, ActionListener oyente){
            JButton btn = new JButton(nombre);
            btn.addActionListener(oyente);
            numeros.add(btn);
        }
    }
}

