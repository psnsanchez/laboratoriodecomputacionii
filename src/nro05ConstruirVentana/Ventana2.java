package nro05ConstruirVentana;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
public class Ventana2 extends JFrame{
    private JButton boton_aceptar;
    private JTextField campo_texto;
    private JLabel etiquetaPanel;
    private JLabel etiquetaBtn;
    private JPanel panel;

    public Ventana2(){
        setSize(800,600);//ancho y alto de la ventana
        setVisible(true);//hacer visible la ventana
        setDefaultCloseOperation(EXIT_ON_CLOSE);//Cerrar ventana y detener programa
        setTitle("Carga de Usuario");//Titulo de la ventana
        setLocationRelativeTo(null);//Posicionar en el centro
        setResizable(true);//prohibir la redimencion del tamaño de la ventana
        getContentPane().setBackground(Color.orange);//establecer colo de fondo
        iniciarComponentes();//llamamos el metodo del Panel
    }
    public void iniciarComponentes(){
        iniciarPanel();
        iniciarBotones();
        iniciarEtiquetas();
        iniciarCampoTexto();
        iniciarMouseListenerPanel();
        iniciarMouseListernerBtn();
        iniciarMouseListernerTextfield();

    }
    private void iniciarMouseListernerTextfield(){
        MouseListener oyenteTextfield = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("campo de texto clickeado");
                campo_texto.setText("Clickeaste");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                System.out.println("campo de texto Presionado");
                campo_texto.setText("Presionaste");
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                System.out.println("campo de texto Soltado");
                campo_texto.setText("Soltaste");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println("campo de texto ingreso");
                campo_texto.setText("Entraste");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                System.out.println("campo de texto Dejado");
                campo_texto.setText("te fuiste");
            }
        };
        campo_texto.addMouseListener(oyenteTextfield);
    }
    private void iniciarMouseListenerPanel(){
        MouseListener oyentePanel = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Mouse clickeado");
                etiquetaPanel.setText("Me Clickesaste");
            }
            @Override
            public void mousePressed(MouseEvent e) {
                System.out.println("Mouse Presionado");
                etiquetaPanel.setText("Me estas Presionando");
            }
            @Override
            public void mouseReleased(MouseEvent e) {
                System.out.println("Mouse Soltado");
                etiquetaPanel.setText("Me Soltaste");
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println("Puntero Entro al panel");
                etiquetaPanel.setText("Entraste a la Ventana");
            }
            @Override
            public void mouseExited(MouseEvent e) {
                System.out.println("Puntero Se fue del panel");
                etiquetaPanel.setText("Te fuiste de la ventana");
            }
        };
        panel.addMouseListener(oyentePanel);
    }
    private void iniciarMouseListernerBtn(){
        MouseListener oyenteBtn = new MouseListener(){
            @Override
            public void mouseClicked(MouseEvent me) {
                System.out.println("Boton clickeado");
                etiquetaBtn.setText("Clickeaste el Boton");
            }
            @Override
            public void mousePressed(MouseEvent me) {
                System.out.println("Boton Apretado");
                etiquetaBtn.setText("Apretaste el Boton");
            }
            @Override
            public void mouseReleased(MouseEvent me) {
                System.out.println("Boton Soltado");
                etiquetaBtn.setText("Soltaste el Boton");
            }
            @Override
            public void mouseEntered(MouseEvent me) {
                System.out.println("Boton sobrevolado");
                etiquetaBtn.setText("Estas sobre el Boton");
            }
            @Override
            public void mouseExited(MouseEvent me) {
                System.out.println("Saliendo del Sobrevolado");
                etiquetaBtn.setText("Saliste del Boton");
            }
        };
        boton_aceptar.addMouseListener(oyenteBtn);
    }
    private void iniciarPanel(){
        panel = new JPanel();//Panel creado pero no implementado
        panel.setBackground(Color.orange);//Establecemos el color del Panel
        getContentPane().add(panel);//Agregamos el panel a la ventana
    }
    private void iniciarBotones(){
        boton_aceptar = new JButton("Aceptar");
        boton_aceptar.setBounds(20,20,20,20);
        panel.add(boton_aceptar);
        /*ActionListener oyente = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                etiqueta1.setText("Hola"+campo_texto.getText()+"Bienvenido");
            }
        };
        boton_aceptar.addActionListener(oyente);*/
    }
    private void iniciarEtiquetas(){
        etiquetaPanel = new JLabel("Texto 1");
        etiquetaPanel.setBounds(30,30,200,200);
        panel.add(etiquetaPanel);

        etiquetaBtn = new JLabel("Texto 2");
        etiquetaBtn.setBounds(30,60,200,200);
        panel.add(etiquetaBtn);
    }
    private void iniciarCampoTexto(){
        campo_texto = new JTextField("    -SETEO POR DEFECTO-    ");
        campo_texto.setBounds(30,90,300,30);
        panel.add(campo_texto);
    }
}