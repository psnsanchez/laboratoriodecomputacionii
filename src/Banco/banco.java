package Banco;

import java.io.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class banco {
    public static void main(String[] args) {
        int nr_cuent=0,menu=0;String nombrecuent=" ";double saldocuent=0,dinero=0;
        Scanner entrada = new Scanner(System.in);

        CuentaCorriente cta1 = new CuentaCorriente(1,"Carlos",23.2);
        CuentaCorriente cta2 = new CuentaCorriente(2,"Axel",10.5);

        Set<CuentaCorriente> listadoDeCeuntas = new HashSet<>();
        listadoDeCeuntas.add(cta1);
        listadoDeCeuntas.add(cta2);


        while (menu != 4){
            System.out.println(" - Menu -\nAgregar cuenta: [1]\nTransferir: [2]\nListar cuentas: [3]");
            menu = entrada.nextInt();
            nr_cuent++;
            switch (menu){
                case 1:
                    System.out.println("\nIngrese nombre: ");
                    nombrecuent = entrada.next();
                    System.out.println("\nIngrese Saldo: ");
                    saldocuent = entrada.nextDouble();
                    listadoDeCeuntas.add(new CuentaCorriente(nr_cuent,nombrecuent,saldocuent));
                    break;
                case 2:
                    System.out.println("Ingrese monto: ");
                    dinero = entrada.nextDouble();
                    operaciones.transferencia(cta1, cta2, dinero);
                    break;
                case 3:
                    try {
                        ObjectOutputStream flujoDeSalida = new ObjectOutputStream(new FileOutputStream(
                                "C:\\Users\\HZ\\Desktop\\practica\\src\\Banco\\listadoDeCeuntas.dat"));
                        flujoDeSalida.writeObject(listadoDeCeuntas);
                        flujoDeSalida.close();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    try {
                        ObjectInputStream flujoDeEntrada = new ObjectInputStream(new FileInputStream(
                                "C:\\Users\\HZ\\Desktop\\practica\\src\\Banco\\listadoDeCeuntas.dat"));
                        Set<CuentaCorriente> listadoDeCeuntasEntrada = (Set<CuentaCorriente>) flujoDeEntrada.readObject();
                        System.out.println("Entrada: "+ listadoDeCeuntasEntrada.toString());
                        flujoDeEntrada.close();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;
                default:
                    System.out.println("Error!");
                    break;
            }

            System.out.println("Si desea salir Ingrese: [4]");
            menu = entrada.nextInt();
        }
    }
}
class CuentaCorriente implements Serializable {
    //Properties
    private int nro_cuenta;
    private String nombre;
    private double saldo;
    //Constructor
    public CuentaCorriente(int nro_cuenta, String nombre, double saldo){
        this.nro_cuenta = nro_cuenta;
        this.nombre = nombre;
        this.saldo = saldo;
    }
    //Getter and Setter
    public int getNro_cuenta() {return nro_cuenta;}
    public void setNro_cuenta(int nro_cuenta) {this.nro_cuenta = nro_cuenta;}
    public String getNombre() {return nombre;}
    public void setNombre(String nombre) {this.nombre = nombre;}
    public double versaldo() {return saldo;}//getSaldo
    public void setSaldo(double saldo) {this.saldo = saldo;}
    //Methods
    public void ingresarDinero(double monto){
        this.saldo = this.saldo + monto;
    }
    public void sacarDinero(double monto){
            this.saldo = this.saldo - monto;
    }
    //toString
    @Override
    public String toString() {
        return "Datos de su cuenta\n"+"Numero de cuenta: "+nro_cuenta+"\nNombre: "+nombre+'\''+"\nsaldo: "+saldo+' ';
    }
}
class operaciones{
    public static void transferencia(CuentaCorriente entrada, CuentaCorriente salida, double monto){
        double a = entrada.versaldo();
        if(a > monto){
            entrada.sacarDinero(monto);
            salida.ingresarDinero(monto);
        }else {
            System.out.println("No cuenta con fondos suficientes!\n");
        }
    }
}