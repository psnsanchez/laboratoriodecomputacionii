package desafios;
import javax.swing.*;
import java.text.DecimalFormat;
public class Desafio8 {
    public static void main(String[] args) {
    int num;
    num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un nuemro: "));
    System.out.println(num);//formato consola
    while (num <= 0){//Cero no permitido para calcular raiz cuadrada
        System.out.println("Error; Usted ingresó un numero menor o igual a 0\n");
        num = Integer.parseInt(JOptionPane.showInputDialog("Intente otra vez: "));
    }
    DecimalFormat df = new DecimalFormat("#.00");
    JOptionPane.showMessageDialog(null,"La raiz de "+num+" es:"+(df.format(Math.sqrt(num))));
    System.out.println("La raiz de "+num+ " es "+(df.format(Math.sqrt(num))));//formato consola
    }
}
