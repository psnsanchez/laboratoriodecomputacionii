package desafios;

public class Desafio7 {
    public static void main(String[] args) {
        double num = Math.toRadians(15);
        System.out.println("Seno de 15: "+Math.sin(num));
        double num2 = Math.toRadians(15);
        System.out.println("Coseno de 15: "+Math.cos(num2));
        double num3 = Math.toRadians(15);
        System.out.println("Tangente de 15: "+Math.tan(num3));
        double num4 = Math.toRadians(15);
        System.out.println("ArcoTangente de 15: "+Math.atan(num4));
        double num5 = Math.toRadians(15);
        System.out.println("Arco2Tangente de 15: "+Math.atan2(num5,num4));
        //----------------------------------------------------------------
        System.out.println("\n------");
        double num6 = Math.exp(2);
        System.out.println("exponente de 2: "+num6);
        double num7 = Math.log(4);
        System.out.println("logaritmo de 4: "+num7);//logaritmo en base e
        //-----------------------------------------------------------------
        System.out.println("\n------");
        double num8 = Math.PI;
        System.out.println("PI: "+num8);
        double num9 = Math.E;
        System.out.println("potencia de 2: "+num9);
        System.out.println("\nsaliendo");
    }
}
