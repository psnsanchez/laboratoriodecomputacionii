package desafios;

public class Desafio4 {
    public static void main(String[] args) {
    //declaracion de variables
        int entero=100;
        double decimal = 3.5;
        char letra = 'l';
        String palabra="hola";
        boolean booleano=true;
    //mostrando por pantalla
        System.out.println("Mostrando valores de variables\n");
        System.out.printf("'"+entero+"' Es un numero entero\n");
        System.out.printf("'"+decimal+"' Es un numero decimal\n");
        System.out.printf("'"+letra+"' Es una letra\n");
        System.out.printf("'"+palabra+"' Es una palabra\n");
        System.out.printf("'"+booleano+"' Es un booleano\n");
        System.out.println("\n-----------------------------\nSaliendo");
    }
}