package desafios;
import java.util.Scanner;
public class Desafio10 {
    public static void main(String[] args) {
        int intentos=0,numero;
        Scanner entrada = new Scanner(System.in);
        int aleatorio = (int) Math.floor(Math.random()*100+1);
        do {
            System.out.println("Ingrese un numero entero entre 1 y 100: ");
            numero = entrada.nextInt();
            while ((numero<=0)||(numero>100)){
                System.out.println("Solo se Admiten numeros enteros, entre 1 y 100\n");
                System.out.println("Ingrese otra vez: ");
                numero = entrada.nextInt();
            }
            if (numero==aleatorio){
                System.out.println("Acertaste el numero, Felicidades!\n");
            }else{
                if (numero>aleatorio){
                    System.out.println("El numero que ingresaste sobrepasa\n");
                }else{
                    System.out.println("El numero que ingresaste es inferior\n");
                }
            }
            intentos++;
        } while (numero!=aleatorio);
        System.out.println("\nEl numero de intentos es: "+intentos);
    }
}
