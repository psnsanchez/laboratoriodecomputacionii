package desafios.desafio11;
import java.util.Random;
class CuentaCorriente{
        //Propiedades
        private double saldo=0;
        private String nombre="Sin nombre";
        private long NumeroCuenta=0;
        //Constructor
        public CuentaCorriente(double saldo, String nombre){
            super();
            this.saldo = saldo;
            this.nombre = nombre;
            Random aleatorio = new Random();
            this.NumeroCuenta = Math.abs(aleatorio.nextLong());
        }
        public CuentaCorriente(){
        super();
        this.saldo = 0;
        this.nombre = "Sin nombre";
        this.NumeroCuenta = 0;
        }
        //Getters And Setters
        public void setSaldo(){
            saldo = this.saldo;
        }
        public double getSaldo(){
           return saldo;
        }
        public void setNombre(){
            nombre = this.nombre;
        }
        public String getNombre(){
            return nombre;
        }
        //Metodos
        public void ingreso(double ingreso){
            if (ingreso>0){
                this.saldo = saldo + ingreso;
                System.out.println("Ingreso de: $"+ingreso+" a la cuenta\n");
            }else{
                System.out.println("No se puede realizar la operacion\n");
            }
        }
        public void extraccion(double monto){
            if(saldo<monto){
                System.out.println("No se puede realizar la operacion\n");
            }else{
                this.saldo = saldo - monto;
            }
        }
        public boolean operacion (CuentaCorriente cuenta1, CuentaCorriente cuenta2, double monto){
            boolean correcto = true;
            if (monto < 0){
                    correcto = false;//monto positivo
            }else{
                if (saldo >= monto){//margen permitido
                    cuenta1.extraccion(monto);
                    cuenta2.ingreso(monto);
                }else{
                    correcto = false;//credito insuficiente
                }
            }
            return correcto;
        }
        //toString
        @Override//sobrescritura de metodo
        public String toString() {
            return "CuentaCorriente \n"+
                    " Nº Cuenta: \t"+NumeroCuenta+
                    "\n nombre: \t'"+nombre+'\''+
                    "\n Saldo: \t$"+saldo+"\n";
        }
}
