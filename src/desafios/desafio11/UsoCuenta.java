package desafios.desafio11;
public class UsoCuenta {
    public static void main(String[]args){
    //inicializacion de 2 instancias
    CuentaCorriente cuenta1 = new CuentaCorriente(2500,"pedro");
    CuentaCorriente cuenta2 = new CuentaCorriente(0,"carlos");
    System.out.println(cuenta1.toString());
    System.out.println(cuenta2.toString());
    double monto = 2500;//Modificable para operar con usuario
    cuenta1.operacion(cuenta1,cuenta2,monto);//llamar al metodo
    System.out.println("\n * Transferencias * \n");
    System.out.println(cuenta1.getNombre()+" -> "+cuenta2.getNombre()+"\nEl monto de: $"+monto+"\n");
    System.out.println(cuenta2.toString());
    }
}
