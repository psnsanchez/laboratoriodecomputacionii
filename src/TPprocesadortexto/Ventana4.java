package TPprocesadortexto;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;

public class Ventana4 extends JFrame{
    public Ventana4(){
        setSize(800,600);
        setTitle("Procesador de texto");
        setBounds(450, 220, 800, 600);
        setResizable(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        Panel panel = new Panel();
        add(panel);
        setVisible(true);
        addWindowListener(new ListenerVentana());
    }
    private class ListenerVentana implements WindowListener{
        @Override
        public void windowOpened(WindowEvent e) {
            // TODO Auto-generated method stub
        }
        @Override
        public void windowClosing(WindowEvent e) {
            if (!Panel3.guardo) {
                int respuesta = JOptionPane.showConfirmDialog(null, "¿Quiere Guardar?", "Saliendo sin guardar", JOptionPane.YES_NO_OPTION);
                if (respuesta == JOptionPane.YES_OPTION) {
                    if (Panel3.guardo) {
                        Panel3.guardadoRapido();
                    } else {
                        Panel3.guardado();
                    }
                }
                else {
                    System.exit(0);
                }
            }
        }

        @Override
        public void windowIconified(WindowEvent e) {
            // TODO Auto-generated method stub
        }
        @Override
        public void windowClosed(WindowEvent e) {
            // TODO Auto-generated method stub
        }

        @Override
        public void windowActivated(WindowEvent e) {
            // TODO Auto-generated method stub
        }
        @Override
        public void windowDeiconified(WindowEvent e) {
            // TODO Auto-generated method stub
        }
        @Override
        public void windowDeactivated(WindowEvent e) {
            // TODO Auto-generated method stub
        }
    }


    public static class Panel extends JPanel {
        //Atributos
        private JMenuBar barra;
        private JMenu archivo;
        private JMenu fuente;
        private JMenu estilo;
        private JMenu tamanio;
        private JMenu color;
        private JMenu Alineacion;
        private JMenuItem arch_abrir;
        private JMenuItem arch_guardar;
        private JMenuItem fuente_arial;
        private JMenuItem fuente_courier;
        private JMenuItem fuente_verdana;
        private JMenuItem fuente_comicSansMS;
        private JMenuItem fuente_timesNewRoman;
        private JMenuItem estilo_negrita;
        private JMenuItem estilo_cursiva;
        private JMenuItem estilo_subrayado;
        private ButtonGroup tamanios;
        private JMenuItem tamanio10;
        private JMenuItem tamanio12;
        private JMenuItem tamanio14;
        private JMenuItem tamanio16;
        private JMenuItem tamanio18;
        private JScrollPane scroll;
        private JButton BtnNegro;
        private JButton BtnRojo;
        private JButton BtnAzul;
        private JButton BtnVerde;
        private JMenuItem Menuizq;
        private JMenuItem Menucentro;
        private JMenuItem Menuder;
        private JMenuItem MenuJust;
        private static String direccion = "";
        private static String direccionStyle = "";
        public static boolean guardo = false;
        private static JTextPane textfield;
        //Constructor
        public Panel() {
            setLayout(new BorderLayout());
            setBackground(Color.GRAY);
            iniciarMenuArchivo();
            iniciarMenuFuente();
            iniciarMenuEstilo();
            iniciarMenutamanios();
            iniciarMenuColor();
            iniciarMenuAlinear();
            iniciarMenuBarra();
            iniciarAreaTexto();
        }
        private void iniciarMenuBarra(){
            //creo la barra Menu
            barra = new JMenuBar();
            //agrego sus contenidos
            barra.add(archivo);
            barra.add(fuente);
            barra.add(estilo);
            barra.add(tamanio);
            barra.add(color);
            barra.add(Alineacion);
            //posiciono la barra arriba
            add(barra, BorderLayout.NORTH);
        }
        private void iniciarMenuArchivo(){
            archivo = new JMenu("Archivo");
            //crear
            arch_abrir = new JMenuItem("Abrir");
            arch_guardar = new JMenuItem("Guardar");
            //agregar
            archivo.add(arch_abrir);
            archivo.add(arch_guardar);
            //agregar listener
            arch_abrir.addActionListener(new Panel.ListenerCarga());
            arch_guardar.addActionListener(new Panel.ListenerGuardadoRapido());
            //agregar acceso rapido CTRL+G
            arch_guardar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_DOWN_MASK));
        }
        private void iniciarMenuEstilo(){
            estilo = new JMenu("Estilo");
            //crear
            estilo_negrita = new JMenuItem("Negrita");
            estilo_negrita.setFont(new Font("arial",1,12));
            estilo_cursiva = new JMenuItem("Cursiva");
            estilo_cursiva.setFont(new Font("arial",2,12));
            estilo_subrayado = new JMenuItem("Subrayado");
            estilo_subrayado.setFont(new Font("arial",0,12));
            //agregar
            estilo.add(estilo_negrita);
            estilo.add(estilo_cursiva);
            estilo.add(estilo_subrayado);
            //agregar acceso rapido CTRL+N CTRL+K y CTRL+U
            estilo_negrita.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
            estilo_negrita.addActionListener(new StyledEditorKit.BoldAction());
            estilo_cursiva.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,InputEvent.CTRL_DOWN_MASK));
            estilo_cursiva.addActionListener(new StyledEditorKit.ItalicAction());
            estilo_subrayado.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_DOWN_MASK));
            estilo_subrayado.addActionListener(new StyledEditorKit.UnderlineAction());
        }
        private void iniciarMenuFuente(){
            fuente = new JMenu("Fuente");
            //crear
            fuente_arial = new JMenuItem("Arial");
            fuente_arial.setFont(new Font("arial",1,10));
            fuente_courier = new JMenuItem("Courier");
            fuente_courier.setFont(new Font("Courier",1,10));
            fuente_verdana = new JMenuItem("Verdana");
            fuente_verdana.setFont(new Font("verdana",1,10));
            fuente_comicSansMS = new JMenuItem("Comic Sans");
            fuente_comicSansMS.setFont(new Font("Comic Sans MS",1,10));
            fuente_timesNewRoman = new JMenuItem("Times New Roman");
            fuente_timesNewRoman.setFont(new Font("Times New Roman",1,10));

            //agregar
            fuente.add(fuente_arial);
            fuente.add(fuente_courier);
            fuente.add(fuente_verdana);
            fuente.add(fuente_comicSansMS);
            fuente.add(fuente_timesNewRoman);
            fuente_arial.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Arial"));
            fuente_courier.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Courier"));
            fuente_verdana.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Verdana"));
            fuente_comicSansMS.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Comic Sans MS"));
            fuente_timesNewRoman.addActionListener(new StyledEditorKit.FontFamilyAction("cambioDeLetra","Times New Roman"));
        }
        private void iniciarMenutamanios(){
            tamanio = new JMenu("Tamaño");
            tamanios = new ButtonGroup();//creo el grupo
            //creacion de los botones
            tamanio10 = new JRadioButtonMenuItem("10");
            tamanio10.setFont(new Font("arial",1,10));
            tamanio12 = new JRadioButtonMenuItem("12");
            tamanio12.setFont(new Font("arial",1,12));
            tamanio14 = new JRadioButtonMenuItem("14");
            tamanio14.setFont(new Font("arial",1,14));
            tamanio16 = new JRadioButtonMenuItem("16");
            tamanio16.setFont(new Font("arial",1,16));
            tamanio18 = new JRadioButtonMenuItem("18");
            tamanio18.setFont(new Font("arial",1,18));
            //agrego los botones a la agrupacion
            tamanios.add(tamanio10);
            tamanios.add(tamanio12);
            tamanios.add(tamanio14);
            tamanios.add(tamanio16);
            tamanios.add(tamanio18);
            //agrego botones al menu
            tamanio.add(tamanio10);
            tamanio.add(tamanio12);
            tamanio.add(tamanio14);
            tamanio.add(tamanio16);
            tamanio.add(tamanio18);
            //agrego Actions Listeners
            tamanio10.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 10));
            tamanio12.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 12));
            tamanio14.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 14));
            tamanio16.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 16));
            tamanio18.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 18));
        }
        private void iniciarMenuColor(){
            color = new JMenu("Colores");
            //crear
                BtnNegro = new JButton();
                BtnRojo = new JButton();
                BtnAzul = new JButton();
                BtnVerde = new JButton();
                BtnNegro.setBackground(Color.BLACK);
                BtnRojo.setBackground(Color.RED);
                BtnAzul.setBackground(Color.BLUE);
                BtnVerde.setBackground(Color.GREEN);
            //agregar
            color.add(BtnNegro);
            color.add(BtnRojo);
            color.add(BtnAzul);
            color.add(BtnVerde);
            //Agregar Action Listener
            BtnNegro.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.BLACK));
            BtnRojo.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.RED));
            BtnAzul.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.BLUE));
            BtnVerde.addActionListener(new StyledEditorKit.ForegroundAction("cambiarColor", Color.GREEN));
        }
        private void iniciarMenuAlinear(){
            Alineacion = new JMenu("Alinear");
            Menuizq = new JMenuItem("Izquierda");
            Menucentro = new JMenuItem("Centro");
            Menuder = new JMenuItem("Derecha");
            MenuJust = new JMenuItem("Justificado");
            Alineacion.add(Menuizq);
            Alineacion.add(Menucentro);
            Alineacion.add(Menuder);
            Alineacion.add(MenuJust);
            Menuizq.addActionListener(new StyledEditorKit.AlignmentAction("cambiarAlineacion", StyleConstants.ALIGN_LEFT));
            Menucentro.addActionListener(new StyledEditorKit.AlignmentAction("cambiarAlineacion", StyleConstants.ALIGN_CENTER));
            Menuder.addActionListener(new StyledEditorKit.AlignmentAction("cambiarAlineacion", StyleConstants.ALIGN_RIGHT));
            MenuJust.addActionListener(new StyledEditorKit.AlignmentAction("cambiarAlineacion", StyleConstants.ALIGN_JUSTIFIED));
        }
        private void iniciarAreaTexto(){
            //creo el campo de texto
            textfield = new JTextPane();
            textfield.setBackground(Color.ORANGE);//me gusta el color
            //agrego un Scroll al campo de texto
            scroll = new JScrollPane(textfield);
            add(scroll, BorderLayout.CENTER);
        }
        //LISTENERS
        private class ListenerGuardadoRapido implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e){
                if (guardo){
                    Panel.guardadoRapido();
                } else{
                    Panel.guardado();
                }
            }
        }
        private class ListenerCarga implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e){
                Panel.carga();
            }
        }
        //Metodos con Trycatch
        public static void carga(){
            try{
                JFileChooser seleccionar = new JFileChooser();
                seleccionar.showOpenDialog(textfield);
                String archivo = seleccionar.getSelectedFile().getAbsolutePath();
                String archivoSyle = seleccionar.getCurrentDirectory() +"\\"+ seleccionar.getSelectedFile().getName() + "Style";
                ObjectInputStream lectura = new ObjectInputStream(new FileInputStream(archivo));
                ObjectInputStream lecturaStyle = new ObjectInputStream(new FileInputStream(archivoSyle));
                textfield.setText((String) lectura.readObject());
                textfield.setStyledDocument((StyledDocument) lecturaStyle.readObject());
                lectura.close();
                lecturaStyle.close();
                guardo = false;
            }
            catch (IOException ioe){
                JOptionPane.showConfirmDialog(null, "Error, algo salio mal!!", "Error", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                ioe.printStackTrace();
                return;
            }
            catch (ClassNotFoundException c){
                System.out.println("Error, Clase no encontrada");
                c.printStackTrace();
                return;
            }
        }
        public static void guardado(){
            try{
                System.out.println("Guardado");
                JFileChooser Archivo = new JFileChooser();
                Archivo.showSaveDialog(textfield);
                File archivo = (File) Archivo.getCurrentDirectory();
                String ruta = archivo+"\\"+Archivo.getSelectedFile().getName();
                String rutaStyle = archivo+"\\"+Archivo.getSelectedFile().getName()+"Style";
                direccion = ruta;
                direccionStyle = rutaStyle;
                ObjectOutputStream Salida = new ObjectOutputStream(new FileOutputStream(ruta) );
                ObjectOutputStream SalidaStyles = new ObjectOutputStream(new FileOutputStream(rutaStyle));
                Salida.writeObject(textfield.getText());
                SalidaStyles.writeObject(textfield.getStyledDocument());
                Salida.close();
                SalidaStyles.close();
                JOptionPane.showConfirmDialog(null,"Archivos Guardados","Guardado Correcto",JOptionPane.CLOSED_OPTION,JOptionPane.PLAIN_MESSAGE);
                guardo = true;
            }
            catch (FileNotFoundException e){
                JOptionPane.showConfirmDialog(null, "Archivo no encontrado!", "Error! en Guardado", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                System.out.println("Error en Guardado");
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        public static void guardadoRapido(){
            try{
                System.out.println("Guardado Rapido");
                ObjectOutputStream Salida = new ObjectOutputStream(new FileOutputStream(direccion) );
                ObjectOutputStream SalidaStyles = new ObjectOutputStream(new FileOutputStream(direccionStyle) );
                Salida.writeObject(textfield.getText());
                SalidaStyles.writeObject(textfield.getStyledDocument());
                SalidaStyles.close();
                Salida.close();
            }
            catch (FileNotFoundException e){
                JOptionPane.showConfirmDialog(null, "Archivo no encontrado!", "Error! en guardado rapido", JOptionPane.CLOSED_OPTION, JOptionPane.ERROR_MESSAGE);
                System.out.println("Error! en Guardado Rapido");
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
